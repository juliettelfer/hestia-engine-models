from hestia_earth.models.log import logRequirements, logShouldRun
from . import MODEL

# TODO: define requirements
REQUIREMENTS = {
    "_required_type_": {

    }
}
# TODO: add lookups if any or remove line
LOOKUPS = {}
# TODO: define return type and content
RETURNS = {
    "_return_type_": [{
        "value": "",
        "methodTier": "tier 1",
        "statsDefinition": "modelled"
    }]
}
TERM_ID = '_term_id_'


def _run(_required_type_lowercase_: dict):
    # TODO: calculate value and return data
    value = 0


def _should_run(_required_type_lowercase_: dict):
    # TODO: define conditions to run the model

    logRequirements(_required_type_lowercase_, model=MODEL, term=TERM_ID,
                    )

    should_run = all([])
    logShouldRun(_required_type_lowercase_, MODEL, TERM_ID, should_run)
    return should_run


def run(_required_type_lowercase_: dict):
return _run(_required_type_lowercase_) if _should_run(_required_type_lowercase_) else []
