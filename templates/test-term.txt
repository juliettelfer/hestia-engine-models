from unittest.mock import patch
import json
from tests.utils import fixtures_path, fake_new__return_type_lowercase_

from hestia_earth.models._model_._term_id_ import MODEL, TERM_ID, run, _should_run

class_path = f"hestia_earth.models.{MODEL}.{TERM_ID}"
fixtures_folder = f"{fixtures_path}/{MODEL}/{TERM_ID}"


def test_should_run():
    _required_type_lowercase_ = {}
    # TODO: define test cases
    assert _should_run(_required_type_lowercase_) is True


@patch(f"{class_path}._new__return_type_lowercase_", side_effect=fake_new__return_type_lowercase_)
def test_run(*args):
    with open(f"{fixtures_folder}/_required_type_lowercase_.jsonld", encoding='utf-8') as f:
        _required_type_lowercase_ = json.load(f)

    with open(f"{fixtures_folder}/result.jsonld", encoding='utf-8') as f:
        expected = json.load(f)

    value = run(_required_type_lowercase_)
    assert value == expected
