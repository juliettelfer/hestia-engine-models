#!/bin/sh
docker build --progress=plain \
  -t hestia-engine-models:test \
  -f tests/Dockerfile \
  .
docker run --rm \
  --env-file .env \
  -v ${PWD}/coverage:/app/coverage \
  -v ${PWD}/hestia_earth:/app/hestia_earth \
  -v ${PWD}/tests:/app/tests \
  hestia-engine-models:test "$@"
