## Freshwater withdrawals, inputs production

Withdrawals of water from freshwater lakes, rivers, and aquifers related to producing the inputs used by this Cycle.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [freshwaterWithdrawalsInputsProduction](https://hestia.earth/term/freshwaterWithdrawalsInputsProduction)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - a [impactAssessment](https://hestia.earth/schema/Input#impactAssessment) with:
        - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
          - [term](https://hestia.earth/schema/Indicator#term) with [freshwaterWithdrawalsDuringCycle](https://hestia.earth/term/freshwaterWithdrawalsDuringCycle) and [value](https://hestia.earth/schema/Indicator#value) `> 0`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('freshwaterWithdrawalsInputsProduction', ImpactAssessment))
```
