## Impact Assessment Post Checks

List of models to run **after** any other model on an `ImpactAssessment`.

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import irrigated
from hestia_earth.models.impact_assessment import post_checks

impact['irrigated'] = irrigated.run(impact)
impact = post_checks.run(impact)
print(impact)
```
