# Dämmgen (2009)

These models calculate direct and indirect greenhouse gas emissions from the German GHG inventory guidelines, [Dämmgen (2009)](https://www.thuenen.de/media/publikationen/landbauforschung-sonderhefte/lbf_sh324.pdf).
