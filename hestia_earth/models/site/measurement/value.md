## Measurement Value

This model calculates the `value` of the [Measurement](https://hestia.earth/schema/Measurement)
by taking an average from the `min` and `max` values.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [value](https://hestia.earth/schema/Measurement#value)

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - [min](https://hestia.earth/schema/Measurement#min) and [max](https://hestia.earth/schema/Measurement#max)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('measurement.value', Site))
```
