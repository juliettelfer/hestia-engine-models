## Organic carbon (per ha)

The stock of organic carbon in the soil.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [organicCarbonPerHa](https://hestia.earth/term/organicCarbonPerHa)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [depthUpper](https://hestia.earth/schema/Measurement#depthUpper)
  - [depthLower](https://hestia.earth/schema/Measurement#depthLower)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `modelled`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `modelled using other physical measurements`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [soilBulkDensity](https://hestia.earth/term/soilBulkDensity) and [depthUpper](https://hestia.earth/schema/Measurement#depthUpper) and [depthLower](https://hestia.earth/schema/Measurement#depthLower)
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [organicCarbonPerKgSoil](https://hestia.earth/term/organicCarbonPerKgSoil) and [depthUpper](https://hestia.earth/schema/Measurement#depthUpper) and [depthLower](https://hestia.earth/schema/Measurement#depthLower)

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('organicCarbonPerHa', Site))
```
