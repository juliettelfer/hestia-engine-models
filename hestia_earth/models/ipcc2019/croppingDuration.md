## Cropping duration

For temporary crops, the period from planting to harvest in days. For perennial crops such as asparagus, the period from planting to removal in days. For transplant rice and other crops with a nursery stage, cropping duration excludes the nursery stage.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [croppingDuration](https://hestia.earth/term/croppingDuration)
  - [methodModel](https://hestia.earth/schema/Practice#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Practice#value)
  - [min](https://hestia.earth/schema/Practice#min)
  - [max](https://hestia.earth/schema/Practice#max)
  - [sd](https://hestia.earth/schema/Practice#sd)
  - [statsDefinition](https://hestia.earth/schema/Practice#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) with [riceGrainInHuskFlooded](https://hestia.earth/term/riceGrainInHuskFlooded) **or** [ricePlantFlooded](https://hestia.earth/term/ricePlantFlooded) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop)

### Lookup used

- [region-ch4ef-IPCC2019.csv](https://hestia.earth/glossary/lookups/region-ch4ef-IPCC2019.csv) -> `Rice_croppingDuration_days`; `Rice_croppingDuration_days_min`; `Rice_croppingDuration_days_max`; `Rice_croppingDuration_days_sd`
- [landUseManagement.csv](https://hestia.earth/glossary/lookups/landUseManagement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('croppingDuration', Cycle))
```
