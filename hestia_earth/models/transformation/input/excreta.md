## Input Excreta

Copy Cycle (or previous Transformation) `excreta` products into the Transformation inputs if they are missing.

### Returns

* A list of [Transformations](https://hestia.earth/schema/Transformation) with:
  - a list of [inputs](https://hestia.earth/schema/Transformation#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
    - [value](https://hestia.earth/schema/Input#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - a list of [transformations](https://hestia.earth/schema/Cycle#transformations) with:
    - [term](https://hestia.earth/schema/Transformation#term) of [termType](https://hestia.earth/schema/Term#termType) = [excretaManagement](https://hestia.earth/glossary?termType=excretaManagement) and a list of [inputs](https://hestia.earth/schema/Transformation#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.transformation import run

print(run('input.excreta', Cycle))
```
