## Energy content (lower heating value)

The amount of heat released by combusting a specified quantity in a calorimiter. The combustion process generates water vapor, but the heat in the water vapour is not recovered and accounted for.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - a list of [properties](https://hestia.earth/schema/Input#properties) with:
    - [term](https://hestia.earth/schema/Property#term) with [energyContentLowerHeatingValue](https://hestia.earth/term/energyContentLowerHeatingValue)
    - [value](https://hestia.earth/schema/Property#value)
    - [statsDefinition](https://hestia.earth/schema/Property#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [woodFuel](https://hestia.earth/glossary?termType=woodFuel) **or** [woodPellets](https://hestia.earth/glossary?termType=woodPellets) and a list of [properties](https://hestia.earth/schema/Input#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [dryMatter](https://hestia.earth/term/dryMatter)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('energyContentLowerHeatingValue', Cycle))
```
