## ecoinvent v3

This model calculates background emissions related to the production of Inputs from the ecoinvent database, version 3.

Note: to use the `ecoinventV3` model locally or in the
[Hestia Community Edition](https://gitlab.com/hestia-earth/hestia-community-edition) you need a valid ecoinvent license.
Please contact us at community@hestia.earth for instructions to download the required file to run the model.

**Pesticide Brand Name**

For `Input` with a [Pesticide Brand Name](https://hestia.earth/glossary?pesticideBrandBane) term, you can override the
default list of [Pesticide Active Ingredient](https://hestia.earth/glossary?pesticideAI) by specifying the list of
[properties](https://hestia.earth/schema/Input#properties) manually.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ecoinventV3](https://hestia.earth/term/ecoinventV3)
  - [term](https://hestia.earth/schema/Emission#term)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `background`
  - [inputs](https://hestia.earth/schema/Emission#inputs)
  - [operation](https://hestia.earth/schema/Emission#operation)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) `> 0`

### Lookup used

- [electricity.csv](https://hestia.earth/glossary/lookups/electricity.csv) -> `ecoinventMapping`
- [fuel.csv](https://hestia.earth/glossary/lookups/fuel.csv) -> `ecoinventMapping`
- [inorganicFertiliser.csv](https://hestia.earth/glossary/lookups/inorganicFertiliser.csv) -> `ecoinventMapping`
- [material.csv](https://hestia.earth/glossary/lookups/material.csv) -> `ecoinventMapping`
- [pesticideAI.csv](https://hestia.earth/glossary/lookups/pesticideAI.csv) -> `ecoinventMapping`
- [soilAmendment.csv](https://hestia.earth/glossary/lookups/soilAmendment.csv) -> `ecoinventMapping`
- [transport.csv](https://hestia.earth/glossary/lookups/transport.csv) -> `ecoinventMapping`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('input.ecoinventV3', Cycle))
```
