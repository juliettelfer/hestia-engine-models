## Water depth

The depth of a water body.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [waterDepth](https://hestia.earth/term/waterDepth)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `spatial`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `geospatial dataset`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('waterDepth', Site))
```
