require('dotenv').config();
const fs = require('fs');
const { join, resolve } = require('path');
const axios = require('axios');
const { writeFile } = fs.promises;

const [type, id, dataState] = process.argv.slice(2);
const API_URL = process.env.API_URL;
const ROOT = resolve(join(__dirname, '../'));

const downloadState = async (nodeType, nodeId, state) => {
  const url = `${API_URL}/${nodeType.toLowerCase()}s/${nodeId}?dataState=${state}`;
  console.log('Trying to download', url);
  const { data } = await axios.get(url);
  return data;
};

const downloadNode = async (nodeType, nodeId, dataState) => {
  try {
    return await downloadState(nodeType, nodeId, dataState);
  }
  catch (err) {
    console.log('Failed to download');
    return state === 'recalculated' ? await downloadState(nodeType, nodeId, 'original') : null;
  }
};

const run = async () => {
  const node = await downloadNode(type, id, dataState || 'recalculated');
  if ('cycle' in node) {
    node.cycle = await downloadNode(node.cycle['@type'], node.cycle['@id'], 'recalculated');
  }
  if ('site' in node) {
    node.site = await downloadNode(node.site['@type'], node.site['@id'], 'recalculated');
  }
  return node ? await writeFile(join(ROOT, 'samples', `${id}.jsonld`), JSON.stringify(node, null, 2), 'utf-8') : null;
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
