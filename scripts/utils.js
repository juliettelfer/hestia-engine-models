require('dotenv').config();
const { readFileSync, readdirSync, lstatSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');
const { S3 } = require('aws-sdk');
const csv = require('csvtojson/v2');

const s3 = new S3();
const ENCODING = 'UTF-8';
const ROOT = resolve(join(__dirname, '../'));
const MODELS_PATH = join('hestia_earth', 'models');
const SRC_DIR = join(ROOT, MODELS_PATH);

const ingoreCache = v => !v.includes('cache');
const isInit = v => v.endsWith('__init__.py');
const isUtils = v => v.startsWith('utils/') || v.endsWith('utils.py');
const unique = (values) => [...new Set(values)];
const modelsDir = readdirSync(SRC_DIR).filter(dir => dir != 'data' && !dir.endsWith('.py'));
const extraModels = ['emissionNotRelevant', 'linkedImpactAssessment'];

const isModel = model => ![
  'cycle', 'site', 'impact_assessment', 'transformation',
  'geospatialDatabase'
].includes(model);

const initFilePath = v => {
  const parts = v.split('/');
  const _f = parts.pop();
  return [...parts, '__init__.py'].join('/');
};

const cleanModel = model => {
  const parts = model.split('/');
  return (
    parts.length === 1 || isModel(parts[0]) ? parts[0] : parts[1]
  );
};

const findModel = content => {
  const val = content.match(/[^_]MODEL(\s?)=(\s?)\'([a-zA-Z-_,0-9]+)\'/g);
  return val && val.length ? val[0].replace(/MODEL|'|=/g, '').trim() : null;
};

const findModelKey = content => {
  const val = content.match(/[^_]MODEL_KEY(\s?)=(\s?)\'([a-zA-Z-_,0-9]+)\'/g);
  return val && val.length ? val[0].replace(/MODEL_KEY|'|=/g, '').trim() : null;
};

const findTerms = content => {
  const val = content.match(/[^_]TERM_ID(\s?)=(\s?)\'([a-zA-Z-_,0-9]+)\'/g);
  return val && val.length ? val[0].replace(/TERM_ID|'|=/g, '').trim().split(',').filter(Boolean) : [];
};

const findModels = dir => {
  const path = join(SRC_DIR, dir);
  const folders = readdirSync(path).filter(v => lstatSync(join(path, v)).isDirectory() && ingoreCache(v));
  const files = readdirSync(path).filter(v => v.endsWith('.py'));
  return [
    ...files.flatMap(file => join(dir, file)),
    ...folders.flatMap(folder => findModels(join(dir, folder)))
  ];
};

const parseJSONData = (content, key) => {
  const keyIndex = content.indexOf(`${key} = {`);
  if (keyIndex === -1) {
    return {};
  }
  const startIndex = keyIndex + key.length + 3;
  const endIndex = startIndex + content.substring(startIndex).search(/\}[\n][\w|\n]/, startIndex) + 1;
  const json = content.substring(startIndex, endIndex);
  return JSON.parse(json);
};

const parseFileModel = filepath => {
  const [model, ...parts] = filepath.split('/');
  return ['pre_checks', 'post_checks'].includes(parts[0])
    ? { model }
    : extraModels.includes(model)
      ? { model, runName: 'all' }
      : parts.length === 1
        ? {
          model,
          runName: parts[0].replace('.py', '')
        }
        : {
          model,
          runName: parts.join('.').replace('.py', '')
        };
};

const listModels = (ignoreInit = false) =>
  modelsDir
    .filter(file => ingoreCache(file) && !file.includes('mocking') && !file.includes('utils'))
    .flatMap(findModels)
    .filter(file => !ignoreInit || !isInit(file));

// comment to ignore line length
const readModelContent = filename => readFileSync(join(SRC_DIR, filename), ENCODING).replace(new RegExp('  # noqa: E501', 'g'), '');

const writeToFile = (filename, data) => writeFileSync(
  filename.startsWith(ROOT) ? filename : join(ROOT, filename)
, data, ENCODING);

const writeJSON = (filename, data) => writeToFile(filename, JSON.stringify(data, null, 2));

const lookupToJson = (filename) => {
  const stream = s3.getObject({
    Bucket: process.env.AWS_BUCKET_GLOSSARY,
    Key: `glossary/lookups/${filename}`
  }).createReadStream();
  return csv().fromStream(stream);
};

module.exports = {
  MODELS_PATH, SRC_DIR, modelsDir, extraModels, ingoreCache,
  unique,
  readModelContent, writeToFile, writeJSON,
  isInit, isUtils,
  cleanModel, isModel, parseJSONData,
  findModel, findModelKey, findTerms, parseFileModel,
  listModels,
  lookupToJson
};
